#this is not sqlalchemy. This is SQLite. Use this if and when SQLalchemy fails to perform
import sqlite3

CREATE_GRAPHDATA_TABLE = "CREATE TABLE IF NOT EXISTS graphData(id INTEGER PRIMARY KEY, totalPopulation INTEGER, contactRate INTEGER, recoveryPeriod INTEGER, risk INTEGER);"

def connect():
    return sqlite3.connect("D:\CodingProjects\disease_spreadlifecycle\package\site.db")




def createTables(connection):
    with connection:
        connection.execute(CREATE_GRAPHDATA_TABLE)

GET_TOTALPOPULATION = "SELECT totalPopulation FROM graphData where id = (SELECT MAX(ID) FROM graphData); "

GET_CONTACTRATE = "SELECT contactRate FROM graphData where id = (SELECT MAX(ID) FROM graphData); "

GET_RECOVERYRATE = "SELECT recoveryPeriod FROM graphData where id = (SELECT MAX(ID) FROM graphData); "

GET_TRANSMISSION_RATE = "SELECT risk FROM graphData where id = (SELECT MAX(ID) FROM graphData); "


def get_latest_tp(connection):
    with connection:
       connection.execute(GET_TOTALPOPULATION).fetchone()

def get_latest_contactRate(connection):
    with connection:
       connection.execute(GET_CONTACTRATE).fetchone()

def get_latest_recoveryPeriod(connection):
    with connection:
       connection.execute(GET_RECOVERYRATE).fetchone()

def get_latest_risk(connection):
    with connection:
       connection.execute(GET_TRANSMISSION_RATE).fetchone()

