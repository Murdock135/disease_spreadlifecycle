from flask_wtf import FlaskForm
from wtforms import IntegerField, SubmitField
from wtforms.validators import ValidationError
# from app import db

class calculateForm(FlaskForm):
    '''we need the following fields.
    contact rate
    risk
    recovery period'''

    contactRate = IntegerField('Contact rate',)
    risk = IntegerField('Risk')
    recoveryPeriod = IntegerField('Recovery Period')
    calculate = SubmitField('Calculate and visualize lifespan')
    totalPopulation = IntegerField('Total Population')

