#Don't forget to install flask and create or activate a venv in the command line
#run the requirements.txt file with pip to install all the dependencies
#Read readme.txt

from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)

app.config['SECRET_KEY'] = '778f32f660'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///site.db'

db = SQLAlchemy(app)

from package import routes