from flask import render_template, redirect, url_for
from package import app,db
from package.forms import calculateForm
from package.calculator import return_fig
from package.models import graphData
from flask_sqlalchemy import SQLAlchemy


@app.route('/', methods = ['GET', 'POST'])
@app.route('/home')
def home():
    form = calculateForm()
    if form.validate_on_submit():
    
        givenData = graphData( totalPopulation = form.totalPopulation.data,
                               contactRate = form.contactRate.data,
                               recoveryPeriod = form.recoveryPeriod.data,
                               risk = form.risk.data
                               )
        db.session.add(givenData)
        db.session.commit()
        latestEntry = db.session.query(graphData).order_by(graphData.id.desc()).first()
        return_fig(latestEntry.totalPopulation, latestEntry.contactRate, latestEntry.risk, latestEntry.recoveryPeriod)
        graph_fig = url_for('static', filename = 'graphfig'+ str(latestEntry.id) + '.png')

        return render_template('graph.html', form = form , title='Your Graph', graph_fig = graph_fig)

    return render_template('layout.html', form = form, title = 'home' )

@app.route('/graph.html', methods = ['GET', 'POST'])
def graph():
    form = calculateForm()
    # df = df
    if form.validate_on_submit():

        givenData = graphData( totalPopulation = form.totalPopulation.data,
                               contactRate = form.contactRate.data,
                               recoveryPeriod = form.recoveryPeriod.data,
                               risk = form.risk.data )
        db.session.add(givenData)
        db.session.commit()
        latestEntry = db.session.query(graphData).order_by(graphData.id.desc()).first()

        return_fig(latestEntry.totalPopulation, latestEntry.contactRate, latestEntry.risk, latestEntry.recoveryPeriod)
        
        graph_fig = url_for('static', filename = 'graphfig'+ str(latestEntry.id) + '.png')
       
        return render_template('graph.html', form = form, title='Your Graph', graph_fig = graph_fig )

    return render_template('graph.html', form = form, title='Your Graph', graph_fig = graph_fig )
