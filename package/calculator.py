import matplotlib.pyplot as plt
import pandas as pd
from scipy.integrate import odeint
import numpy as np
from flask_sqlalchemy import SQLAlchemy
from package.models import graphData
from package import db



def deriv(state, t, N, beta, gamma):
    S,I,R = state
    #change in S population over time
    dSdt = -beta*S*I/N
    #change in I population over time
    dIdt = beta*S*I/N-gamma*I
    #change in R population over time
    dRdt = gamma*I
    
    return dSdt,dIdt,dRdt

def get_latestEntry():
    return db.session.query(graphData).order_by(graphData.id.desc()).first()


def return_fig(total_pop, contact_rate, transmission_number, recovery_time):
    
    # contact_rate = latestEntry.contactRate
    # total_pop = latestEntry.totalPopulation
    # transmission_number = latestEntry.risk
    # recovery_time = latestEntry.recoveryPeriod

    transmission_rate = transmission_number/100
    ecr = transmission_rate*contact_rate
    recoveryRate = 1/recovery_time
    recovered = 0
    infected = 1
    susceptible = total_pop - infected - recovered


    # # sample usage
    # effectiveContactRate = 0.05*5
    # recoveryRate = 1/4
    # print("R0 is {}".format(R0 = effectiveContactRate/recoveryRate))

    # tp = 1000
    # recovered = 0
    # infected = 1
    # susceptiple = tp - recovered - infected

    days = range(1, 130)

    ret = odeint(deriv, [ susceptible, infected, recovered ], days, args = ( total_pop, ecr, recoveryRate ) )

    S, I, R = ret.T #spews out list of S,I and R people on each of the days from 1 to 160 

    df = pd.DataFrame({ 'susceptible':S , 'infected':I , 'recovered':R , 'day':days})

    # plt.style.use('ggplot')
    # df.plot(x='day',
    #         y=['infected', 'susceptible', 'recovered'],
    #         color=['#bb6424', '#aac6ca', '#cc8ac0'],
    #         kind='area',
    #         stacked=False)

    # graph = []
    # graph.append( df.plot(x='day',
    #                       y=['infected', 'susceptible', 'recovered'],
    #                       color=['#bb6424', '#aac6ca', '#cc8ac0'],
    #                       kind='area',
    #                       stacked=False
    #                       )
    #             )
    # figure = []
    # figure.append(dict(data = graph))
    plot = df.plot(x='day',
                        y=['infected', 'susceptible', 'recovered'],
                        color=['#bb6424', '#aac6ca', '#cc8ac0'],
                        kind='area',
                        stacked=False
                        )

    fig = plot.get_figure()
    latestEntry = get_latestEntry()
    fig.savefig('D:\CodingProjects\DiseaseModelling/package/static/graphfig'+ str(latestEntry.id) + '.png', bbox_inches = 'tight')

    
    