from package import app






if __name__ == "__main__":
    app.run(debug=True)
'''if we run it with python in the command line, it should come
to this conditional and run the app'''

'''this conditional will not be true if our module is imported
to somewhere else'''