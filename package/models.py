from package import db

class graphData(db.Model):
    '''need totalpopulation,
    contact rate
    recovery period'''
    id = db.Column(db.Integer, primary_key=True)
    totalPopulation = db.Column(db.Integer, unique = False, nullable = False)
    contactRate = db.Column(db.Integer, unique = False, nullable = False)
    recoveryPeriod = db.Column(db.Integer, unique = False, nullable = False)
    risk = db.Column(db.Integer, unique = False, nullable = False)

    def __repr__(self):
        return  f"graph data ('{self.totalPopulation}', '{self.contactRate}', ' {self.recoveryPeriod}', '{self.risk}' )"